﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ManagerFamiglie))]
public class ManagerFamigliaInterface : Editor
{
    public override void OnInspectorGUI()
    {
        GUILayout.BeginVertical("HelpBox");
        GUILayout.Label("Componente usato per gestire le famiglie di materiali presenti nel programma.\nFamiglie e Sottofamiglie devono avere le cartelle con rispettivi nomi\nall'interno della cartella " +
            "Asset\\Resources.\nAll'interno del campo animatore va messo il Canvas Principale con tanto del\nsuo componente Animator.");
        GUILayout.EndVertical();
        base.OnInspectorGUI();
        if (GUILayout.Button("Aggiorna"))
        {
            ManagerFamiglie.instance.AggiornaTutti();
        }
    }
}
