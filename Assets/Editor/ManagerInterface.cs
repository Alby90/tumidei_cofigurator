﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Manager))]
public class ManagerInterface : Editor
{
    public override void OnInspectorGUI()
    {
        GUILayout.BeginVertical("HelpBox");
        GUILayout.Label("Componente che gestisce quasi tutti i behaviour del programma.\nQui vanno messi riferimenti" +
            "ad elementi importanti quali il principale canvas\ne la cartella dei modelli disponibili.");
        GUILayout.EndVertical();
        base.OnInspectorGUI();
    }
}
