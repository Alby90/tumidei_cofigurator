﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationBehaviour : MonoBehaviour {
	[Header("PARAMETRI ANIMAZIONE")]
	public bool IsAnimatable=false;
	public bool MaterialFromParent = true;
	public Vector3 distance;
	Vector3 startlocation, endlocation;
	bool open;
	IEnumerator isrunning;
	public Vector3 angle;
	public float Speed=3;
	public Material materiale;//variabile temporanea di test
	Quaternion startrotation,endrotation;
	GameObject ParentObject;
	

	void Start () {

		startlocation= this.transform.localPosition;
		startrotation=this.transform.localRotation;
		open=false;
		isrunning=null;
	}

	//WRAPPER per button UI
	public void MoveObject()
	{ 	
		if ( isrunning==null && this.GetComponent<AnimationBehaviour>().IsAnimatable==true) 
		{
			StartCoroutine("Move");
			isrunning = Move();
		}
	}

	// Coroutine di movimento/rotazione
	  IEnumerator Move ()
    {	
		if (open==false)
		{
			endlocation= startlocation+distance;
			endrotation=startrotation *Quaternion.Euler(angle);
		}
		else 
		{
			endlocation=startlocation;
			endrotation=startrotation;
		}
		while((Mathf.Abs(Vector3.Distance(transform.localPosition,endlocation))>0.005f)||(Quaternion.Angle(transform.localRotation, endrotation) > 1f))
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, endlocation,  Time.deltaTime*Speed);
			transform.localRotation = Quaternion.Slerp (transform.localRotation, endrotation,  Time.deltaTime*Speed);
            yield return null;
        }
		transform.localPosition=endlocation;
		transform.localRotation=endrotation;

		if( endlocation==startlocation && endrotation==startrotation) open=false;
		else open=true;
		isrunning=null;
	}
	
}
