﻿//=========================== IMERGO ==========================================
//
// Purpose: menu handler
//
//=============================================================================
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class  MainSelect : MonoBehaviour {
	
	public static MainSelect Instance;
[HideInInspector]
	public  GameObject Selection;
	GameObject TemporalSelection;
	public Text testo;
	
	public GameObject Menu;

   private void Awake()
   {
	   Menu.SetActive(false);
	   	if (Instance==null)
		{ 
			Instance=this;
			DontDestroyOnLoad(gameObject);
		}else
		{
			Destroy(gameObject);
		}
   }
	
	void Update () {
		if (Input.GetMouseButtonUp(0))
         {
             RaycastHit hit;
			 //se trova un oggetto con box collider...
             if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition),out hit))
             {	
				 Menu.SetActive(true);
				 TemporalSelection= hit.collider.gameObject;
				// 1) se l'oggetto non ha un component OUTLINE, la prima volta glielo attacco e lo setto
				 if (TemporalSelection.GetComponent<cakeslice.Outline>() == null)
                        {
							//inserire component Outline...
                            cakeslice.Outline sc = TemporalSelection.AddComponent<cakeslice.Outline>() as cakeslice.Outline;
							TemporalSelection.GetComponent<cakeslice.Outline>().enabled=false;
                    		Debug.Log("inserito component");
                        }
                // 2) se la selezione è di un nuovo oggetto...
				if (TemporalSelection != Selection)
				{	
					// tolgo outline da vecchio se è diverso da null...
					if (Selection != null) Selection.GetComponent<cakeslice.Outline>().enabled=false;
					// faccio outline in nuovo e lo metto come Selected
					Selection=TemporalSelection;
					Selection.GetComponent<cakeslice.Outline>().enabled=true;
					Debug.Log(Selection.name);
			 	}
				else 
				{
					Menu.SetActive(false);
					// 3) se seleziono lo stesso oggetto, tolgo outline e lo metto Selected a null
					TemporalSelection.GetComponent<cakeslice.Outline>().enabled=false; 
					Selection=null;
				}
             }
         }
	
		
	}
	public void PutMaterial(Material num)
		{
			Selection.GetComponent<Renderer>().material= num;
		}	

	public void OpenComposition()
		{
			Animation anim;
			anim= Selection.GetComponent<Collider>().transform.root.GetComponent<Animation>();
         	
         	anim.Play("off");
			Debug.Log("ciao");
			//Selection.collider.transform.root.GetComponent<Animator>().
			
		}	
		public void CloseComposition()
		{
			Animation anim;
			anim= Selection.GetComponent<Collider>().transform.root.GetComponent<Animation>();
         	//anim.speed = 0f;
         	anim.Play("on");Debug.Log("ciao2");
			//Selection.collider.transform.root.GetComponent<Animator>().
			
		}	

}
