﻿
//=========================== IMERGO ==========================================
//
// Purpose: Create materials arrays from Resource folder.
//
//=============================================================================
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadMaterials : MonoBehaviour {

	public static Material[] MaterialCasse;
	public static Material[] MaterialTop;
	//public static Sprite[] IconCasse;
	//public static Sprite[] IconTop;
	public GameObject MatCasseList;
	public GameObject MatTopList;
	public GameObject SampleIcon;
	
	
	Texture TempTex;
	
	void Awake(){
		//lettura e caricamento materiali
		MaterialCasse = Resources.LoadAll<Material> ("materials/cassa");
		MaterialTop = Resources.LoadAll<Material> ("materials/top");
		
		Debug.Log (MaterialCasse.Length);
		
		for (int i = 0; i < MaterialCasse.Length;i++) {
			GameObject obj = Instantiate(SampleIcon) as GameObject;
			
			obj.transform.SetParent(MatCasseList.transform,false); 
			obj.transform.localScale = new Vector3(1, 1, 1);
			//aggiorno il testo del pulsante
			Text description= obj.GetComponentInChildren(typeof(Text)) as Text;
			description.text = MaterialCasse[i].name;
			// taglio l'immagine per creare un icona
			TempTex=MaterialCasse[i].mainTexture;
			obj.GetComponent<RawImage>().texture=TempTex;
			obj.GetComponent<RawImage>().uvRect= new Rect(0f,0f,0.2f,0.2f);
			
			//aggiorno il behaviour  del pulsante
			Button click =obj.GetComponent<Button>();
			Material TempMat=MaterialCasse[i];
			click.onClick.AddListener(() => PutMaterial(TempMat));
			Debug.Log(MaterialCasse[i].name);
			 
		}
		for (int i = 0; i < MaterialTop.Length;i++) {
			GameObject obj = Instantiate(SampleIcon) as GameObject;
			
			obj.transform.SetParent(MatTopList.transform,false); 
			obj.transform.localScale = new Vector3(1, 1, 1);
			//aggiorno il testo del pulsante
			Text description= obj.GetComponentInChildren(typeof(Text)) as Text;
			description.text = MaterialTop[i].name;
			TempTex=MaterialTop[i].mainTexture;
			obj.GetComponent<RawImage>().texture=TempTex;
			obj.GetComponent<RawImage>().uvRect= new Rect(0f,0f,0.2f,0.2f);
			
			//aggiorno il behaviour  del pulsante
			Button click =obj.GetComponent<Button>();
			Material TempMat=MaterialTop[i];
			click.onClick.AddListener(() => PutMaterial(TempMat));
			Debug.Log(MaterialTop[i].name);
			 
		}
		

	}
	void Start(){
		

	}
	void PutMaterial(Material num){
		//crea lista figli
		Transform[] allChildren = MainSelect.Instance.Selection.GetComponentsInChildren<Transform>();
		//cambio il materiale al padre.....
		MainSelect.Instance.Selection.GetComponent<Renderer>().material= num;
		//...e per ogni figlio, se esiste il component...gli cambio il materiale
		foreach (Transform child in allChildren)
		{
			if(child.gameObject.GetComponent<AnimationBehaviour>())	child.gameObject.GetComponent<Renderer>().material= num;
		}
		
		
		
	}

}
