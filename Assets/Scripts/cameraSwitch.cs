﻿using UnityEngine;
using System.Collections;

public class cameraSwitch : MonoBehaviour {
	
	public GameObject[] cameralist;

    private Vector3 acc;
    private Vector3 force;

    public float mass = 1.0f;
    public float dampen = 0.5f;
    public float maxSpeed = 20.0f;

    private Camera cam;

    private Vector3 moveTo, moveFrom;
	private Quaternion rotationTo, rotationFrom;

	private float fovTo, fovFrom, nearClipTo, nearClipFrom, farClipTo, farClipFrom, lerp;
	private int index=0;

	// Use this for initialization
	void Start () {

		cam = this.GetComponent<Camera> ();

        force = new Vector3(0, 0, 0);

        transform.position = moveTo = moveFrom = cameralist[0].transform.position;
		transform.rotation = rotationTo = rotationFrom = cameralist [0].transform.rotation;

		cam.fieldOfView = fovTo = fovFrom = cameralist [0].GetComponent<Camera> ().fieldOfView;
		cam.nearClipPlane= nearClipTo = nearClipFrom = cameralist [0].GetComponent<Camera> ().nearClipPlane;
		cam.farClipPlane= farClipTo = farClipFrom = cameralist [0].GetComponent<Camera> ().farClipPlane;

	}
	
	// Update is called once per frame
	void LateUpdate () {
		
		if (Input.GetKeyDown("q")){
			Next ();
		}

        acc = moveTo - transform.position;
        if(acc.magnitude > maxSpeed)
        {
            acc.Normalize();
            acc *= maxSpeed;
        }
        acc = acc * (1.0f / mass);

        force = force * dampen + acc * (1-dampen);
        transform.position += force;

		if (Vector3.Distance (moveFrom, moveTo) != 0) lerp = 1 - Mathf.Abs (Vector3.Distance (transform.position, moveTo) / Vector3.Distance (moveFrom, moveTo));
		else lerp = 1;

       	Mathf.Clamp01(lerp);

       	transform.rotation = Quaternion.Lerp (rotationFrom, rotationTo, lerp);

		cam.fieldOfView = Mathf.Lerp (fovFrom, fovTo, lerp);
		cam.nearClipPlane = Mathf.Lerp (nearClipFrom, nearClipTo, lerp);
		cam.farClipPlane = Mathf.Lerp (farClipFrom, farClipTo, lerp);
	}


	public void Next(){
		
		index++;

		if (index > cameralist.Length - 1)
			index = 0;

        moveFrom = transform.position;
        moveTo = cameralist [index].transform.position;

        rotationFrom = transform.rotation;
		rotationTo = cameralist [index].transform.rotation;

        fovFrom = transform.GetComponent<Camera>().fieldOfView;
		fovTo = cameralist [index].GetComponent<Camera> ().fieldOfView;

        nearClipFrom = transform.GetComponent<Camera>().nearClipPlane;
		nearClipTo = cameralist [index].GetComponent<Camera> ().nearClipPlane;

        farClipFrom = transform.GetComponent<Camera>().farClipPlane;
        farClipTo = cameralist [index].GetComponent<Camera> ().farClipPlane;
	}
}
