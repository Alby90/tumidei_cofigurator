﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonModel : MonoBehaviour
{
    public GameObject modello;
    Text nome;
    // Start is called before the first frame update
    void Awake()
    {
        nome = GetComponentInChildren<Text>();
    }

    public void settaButton()
    {
        if (modello != null)
        {
            nome.text = modello.name.ToString();
        }
    }

    public void CaricaModello()
    {
        var modName = Manager.instance.modelloAttivo.name;
        print("modello attualmente attivo " + modName);
        if (modello.name != modName)
        {
            Debug.Log("Il modello associato al button ha un nome diverso da quello del modello attivo. Disattivo il modello attivo.");
            Manager.instance.modelloAttivo.SetActive(false);

            print("Cerco all'interno dei modelli in scena se c'è già il modello associato al button");
            foreach (GameObject go in Manager.instance.modelliInScena)
            {
                if(go.name == modello.name)
                {
                    print("Confrontiamo " + go.name + " con " + modello.name+". Hanno lo stesso nome quindi setto come attive "+go.name);
                    go.SetActive(true);
                    Manager.instance.modelloAttivo = go;
                    AlCambioOggetto();
                    print("Esco dalla funzione!");
                    return;
                }
            }

            var modelloGO = Instantiate(modello);
            modelloGO.transform.SetParent(Manager.instance.posizioneSpawn);
            modelloGO.name = modello.name;
            Manager.instance.modelloAttivo = modelloGO;
            Manager.instance.modelliInScena.Add(modelloGO);
            AlCambioOggetto();
        }
        else
        {
            Debug.Log("Il modello associato al button ha lo stesso nome modello attivo. Non faccio nulla.");
        }
    }

    public void AlCambioOggetto()
    {
        if (Manager.instance.oggettoAttivo != null)//Se abbiamo un oggetto attivo...
        {
            if (Manager.instance.oggettoAttivo.GetComponent<cakeslice.Outline>().enabled)//...se questo oggetto ha l'outline attivato
            {
                Manager.instance.oggettoAttivo.GetComponent<cakeslice.Outline>().enabled = false; //...e spegniamo l'outline.
            }
        }
        Manager.instance.oggettoAttivo = null;
        Manager.instance.CloseAnyWindow(true);
    }
}
