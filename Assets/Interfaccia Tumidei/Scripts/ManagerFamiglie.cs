﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ManagerFamiglie : MonoBehaviour
{
    public delegate void Aggiornamento(List<Famiglia> famig);
    public static Aggiornamento aggiorna;
    public List<Famiglia> famiglie;
    public static ManagerFamiglie instance;

    public void OnEnable()
    {
        instance = this;
    }

    public void AggiornaTutti()
    {
        aggiorna(famiglie);
    }
}
