﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button), typeof(Image))]
public class ButtonSub : MonoBehaviour
{
    [HideInInspector]
    public GameObject matPanel, panelSub;
    Button back;

    public void Start()
    {
        back = Manager.instance.backButton.GetComponent<Button>();
    }

    /// <summary>
    /// Questa funzione attiva il panel giusto appartenente alla sottoFamiglia esatta
    /// </summary>
    public void AttivaSubPanel()
    {
        Manager.instance.sonoInSubFam = false;
        if (!matPanel.activeSelf)
        {
            matPanel.SetActive(true);
            transform.parent.gameObject.SetActive(false);
        }
        back.gameObject.SetActive(true);
        Manager.instance.sonoInSubFam = false;
        Manager.instance.materialPanelAttuale = matPanel;
    }
}
