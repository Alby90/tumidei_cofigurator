﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartApp : MonoBehaviour
{
    public GameObject circle;
    Vector3 rotationEuler;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Starting());
    }

    private void Update()
    {
        rotationEuler += Vector3.forward * -30 * Time.deltaTime; //increment 30 degrees every second
        circle.transform.rotation = Quaternion.Euler(rotationEuler);
    }

    IEnumerator Starting()
    {
        yield return new WaitForSeconds(0.25f);
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(1);
        while (!asyncLoad.isDone)
        {
            print(asyncLoad.progress);
            yield return null;
        }
    }
}
