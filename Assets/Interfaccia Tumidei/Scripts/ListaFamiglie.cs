﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ListaFamiglie
{
    [HideInInspector]
    public string nome;
    public bool compatibile;
    public List<ListaSottoFamiglie> sottoFamiglie;

    public ListaFamiglie(string nom, bool comp)
    {
        nome = nom;
        compatibile = comp;
    }
}
