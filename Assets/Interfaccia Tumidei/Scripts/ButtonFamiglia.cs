﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button), typeof(Image))]
public class ButtonFamiglia : MonoBehaviour
{
    public GameObject subPanel;


    public void AttivaSubPanel()
    {
        if(!subPanel.activeSelf)
        {
            Manager.instance.subFamAttuale = subPanel;
            Manager.instance.panelFamiglieGO.SetActive(false);
            subPanel.SetActive(true);
            Manager.instance.sonoInSubFam = true;
        }
    }
}
