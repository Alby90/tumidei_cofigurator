﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button), typeof(Image))]
public class ButtonMateriale : MonoBehaviour
{
    public Material materiale;
    public Image immagine;
    Sprite mySprite;


    /// <summary>
    /// Questa funzione va chiamata alla creazione del button, dopo avergli dato un materiale e serve per impostare la grafica del button Materiale estraendo la texture principale del materiale
    /// </summary>
    public void setMaterialColorButton()
    {
        if(materiale != null && immagine != null)
        {
            if(materiale.mainTexture != null)
            {
                mySprite = Sprite.Create((Texture2D)materiale.mainTexture, new Rect(0.0f, 0.0f, 1, 1), new Vector2(0.5f, 0.5f), 100.0f);
                immagine.sprite = mySprite;
            }
            else
            {
                Color colore = materiale.color;
                immagine.color = colore;
            }

        }
    }


    /// <summary>
    /// Questa funzione applica il materiale affidato al button
    /// </summary>
    public void ApplicaMaterialUno()
    {
        Manager.instance.oggettoAttivo.GetComponent<MeshRenderer>().material = materiale;
    }

    /// <summary>
    /// Questa funzione applica il materiale affidato al button
    /// </summary>
    public void ApplicaMaterialTutti()
    {
        foreach (Transform oggetto in Manager.instance.modelloAttivo.transform)
        {
            if(oggetto.GetComponent<Modificabile>())
            {
                bool compatibile = true;
                var modificabileOggAttivo = Manager.instance.oggettoAttivo.GetComponent<Modificabile>();
                var modificabileOgg = oggetto.GetComponent<Modificabile>();
                for (int i = 0; i<ManagerFamiglie.instance.famiglie.Count; i++)
                {
                    if (modificabileOggAttivo.listFam[i].compatibile != modificabileOgg.listFam[i].compatibile)
                    {
                        compatibile = false;
                        break;
                    }
                }
                if(compatibile)
                {
                    oggetto.GetComponent<MeshRenderer>().material = materiale;
                }
            }
        }
        Manager.instance.oggettoAttivo.GetComponent<MeshRenderer>().material = materiale;
    }

    public void TryApplicaMaterial()
    {
        if(Manager.instance.ApplicaATutti)
        {
            Manager.instance.pannelloAvviso.SetActive(true);
            Manager.instance.pannelloAvviso.transform.GetChild(1).GetComponent<ButtonMateriale>().materiale = materiale;
        }
        else
        {
            Manager.instance.oggettoAttivo.GetComponent<MeshRenderer>().material = materiale;
        }
    }
}
