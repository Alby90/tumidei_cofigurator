﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class Manager : MonoBehaviour
{
    public static Manager instance;
    public Animator animatore;
    public Transform posizioneSpawn;
    //public List<Famiglia> famiglie = new List<Famiglia>();
    public string NomeCartellaModelli;
    public GameObject buttonFamiglia, buttonSottoFamiglia, buttonMaterial, buttonModello, panelPrefab, canvasPrincipale, checkApplica, buttonBASE;
    List<GameObject> panelSottoFamiglie, panelMateriali;
    public AudioClip[] clipAudio;
    [HideInInspector]
    public GameObject oggettoAttivo = null, modelloAttivo;
    [HideInInspector]
    public List<GameObject> bottoniFamiglia, pannelliSub, pannelliMaterial, modelliInScena;
    [HideInInspector]
    public GameObject materialiGO, sottofamiglieGO, panelFamiglieGO, buttonExitGO, pannelloAvviso, backButton, modelPanel, subFamAttuale, materialPanelAttuale;
    public bool ApplicaATutti, sonoInSubFam;
    EventSystem es; //L'event system serve per capire se stiamo sopra ad un elemento UI mentre clicchiamo

    private void Awake()
    {
        instance = this;
        panelFamiglieGO = GameObject.FindGameObjectWithTag("PanelFamiglieGO");
        sottofamiglieGO = GameObject.FindGameObjectWithTag("SottofamiglieGO");
        materialiGO = GameObject.FindGameObjectWithTag("MaterialiGO");
        buttonExitGO = GameObject.FindGameObjectWithTag("ExitButton");
        pannelloAvviso = GameObject.FindGameObjectWithTag("AvvisoPanel");
        backButton = GameObject.FindGameObjectWithTag("BackButton");
        modelPanel = GameObject.FindGameObjectWithTag("ModelPanel");
        buttonExitGO.GetComponent<Button>().onClick.AddListener(() => CloseAnyWindow(true));

        panelFamiglieGO.SetActive(false);
        buttonExitGO.SetActive(false);
        pannelloAvviso.SetActive(false);
        backButton.SetActive(false);
    }

    // Start is called before the first frame update
    void Start()
    {
        es = GameObject.FindGameObjectWithTag("EventSystem").GetComponent<EventSystem>();
        modelloAttivo = posizioneSpawn.GetChild(0).gameObject;
        modelliInScena.Add(modelloAttivo);
        Populate();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if(!es.IsPointerOverGameObject())
            {
                RaycastHit hit;
                //se trova un oggetto con box collider...
                if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
                {
                    //Se questo oggetto ha uno script chiamato "Modificabile"
                    if (hit.collider.gameObject.GetComponent<Modificabile>() != null)
                    {
                        var modificabile = hit.collider.gameObject.GetComponent<Modificabile>();
                        //Se questo oggetto è diverso dall'ultimo selezionato
                        if (hit.collider.gameObject != oggettoAttivo)
                        {
                            CloseAnyWindow(false);
                            if (oggettoAttivo != null)
                            {
                                oggettoAttivo.GetComponent<cakeslice.Outline>().enabled = false;
                            }
                            //Attiva il panel famiglia (non prima di aver attivato/disattivato i buttonfamiglia giusti)
                            for (int i = 0; i < bottoniFamiglia.Count; i++)
                            {
                                if (modificabile.listFam/*famigliaDiMaterialiDisponibile*/[i].compatibile == true)
                                {
                                    bottoniFamiglia[i].SetActive(true);
                                }
                                else
                                {
                                    bottoniFamiglia[i].SetActive(false);
                                }
                            }
                            panelFamiglieGO.SetActive(true);
                            buttonExitGO.SetActive(true);
                            oggettoAttivo = hit.collider.gameObject;
                            oggettoAttivo.GetComponent<cakeslice.Outline>().enabled = true;
                            /*if (Camera.main.GetComponent<AudioSource>().clip != clipAudio[1])
                            {
                                Camera.main.GetComponent<AudioSource>().clip = clipAudio[1];
                            }*/
                            Camera.main.GetComponent<AudioSource>().Play();
                        }
                        else
                        {
                            if (panelFamiglieGO.activeSelf == false)
                            {
                                panelFamiglieGO.SetActive(true);
                                buttonExitGO.SetActive(true);
                                oggettoAttivo.GetComponent<cakeslice.Outline>().enabled = true;
                                /*if (Camera.main.GetComponent<AudioSource>().clip != clipAudio[1])
                                {
                                    Camera.main.GetComponent<AudioSource>().clip = clipAudio[1];
                                }*/
                                Camera.main.GetComponent<AudioSource>().Play();
                                CloseAnyWindow(false);
                            }
                            else
                            {
                                oggettoAttivo.GetComponent<cakeslice.Outline>().enabled = false;
                                CloseAnyWindow(true);
                            }
                        }
                    }
                }
            }
            else
            {
                //Camera.main.GetComponent<AudioSource>().clip = clipAudio[0];
            }
        }
        ApplicaATutti = checkApplica.GetComponent<Toggle>().isOn;
    }

    /// <summary>
    /// Questa funzione, da chiamare all'inizio del programma, si occupa di riempire la UI con i button e i panel necessari
    /// </summary>
    public void Populate()
    {
        //Riempiamo il pannello dei vari modelli con un button per ogni modello presente all'interno della cartella Modelli di Resources
        var modelliMobili = Resources.LoadAll<GameObject>(NomeCartellaModelli);
        foreach (GameObject go in modelliMobili)
        {
            var but = Instantiate(buttonModello);
            but.GetComponent<ButtonModel>().modello = go;
            but.transform.SetParent(modelPanel.transform);
            but.GetComponent<ButtonModel>().settaButton();
            but.GetComponent<Button>().onClick.AddListener(() => Camera.main.GetComponent<AudioSource>().Play());
        }

        foreach (Famiglia fam in ManagerFamiglie.instance.famiglie) //Per ogni famiglia nel database ManagerFamiglie
        {
            var famBut = Instantiate(buttonFamiglia); //Creeiamo un button famiglia
            bottoniFamiglia.Add(famBut); //Aggiungiamo il button appena creato alla lista bottoniFamiglia
            famBut.name = fam.nome; //diamo al button il nome della famiglia alla quale sarà collegato
            famBut.transform.GetComponentInChildren<Text>().text = fam.nome; //Prendiamo dal gameobject Text figlio del button il suo component Text
            famBut.transform.SetParent(panelFamiglieGO.transform); //mettiamo il button come figlio del panelFamiglie
            var subFamPanel = Instantiate(panelPrefab); //creeiamo un pannello sottofamiglie
            famBut.GetComponent<ButtonFamiglia>().subPanel = subFamPanel;  //linkiamo il panel sottofamiglia al button famiglia 
            famBut.GetComponent<Button>().onClick.AddListener(() => Camera.main.GetComponent<AudioSource>().Play());
            famBut.GetComponent<Button>().onClick.AddListener(() => backButton.SetActive(true));
            subFamPanel.name = "PanelSottofamiglia_" + fam.nome; //rinominiamo il panel sottofamiglia con il nome della famiglia
            subFamPanel.transform.SetParent(sottofamiglieGO.transform); //mettiamo il panel come figlio del gameobject sottofamiglieGO
            subFamPanel.GetComponent<ScrollRect>().viewport = canvasPrincipale.GetComponent<RectTransform>();
            pannelliSub.Add(subFamPanel); //Aggiungiamo il panel sottofamiglia alla lista pannelliSub

            //Qui settiamo le dimensioni del pannello appena creato nel canvas
            UtilityRect(subFamPanel, 10);

            if (fam.sottofamiglie.Count != 0) //Se la famiglia NON ha 0 sottofamiglie...
            {
                foreach (Sottofamiglia subFam in fam.sottofamiglie) //...per ogni sottofamiglia
                {
                    //Crea e gestisci un button per ogni sottofamiglia dell'attuale famiglia
                    var btnnSub = Instantiate(buttonSottoFamiglia);
                    btnnSub.name = subFam.nome;
                    var bttnSubScript = btnnSub.GetComponent<ButtonSub>();
                    btnnSub.GetComponentInChildren<Text>().text = subFam.nome;
                    btnnSub.transform.SetParent(subFamPanel.gameObject.transform);
                    btnnSub.GetComponent<Button>().onClick.AddListener(() => Camera.main.GetComponent<AudioSource>().Play());

                    //Creazione panel per i materiale dell'attuale sottofamiglia processata
                    var panMaterial = Instantiate(panelPrefab);
                    panMaterial.transform.SetParent(materialiGO.transform);
                    panMaterial.GetComponent<ScrollRect>().viewport = canvasPrincipale.GetComponent<RectTransform>();
                    panMaterial.name = "PanelMateriali_" + subFam.nome;
                    pannelliMaterial.Add(panMaterial);
                    //Qui settiamo le dimensioni del pannello appena creato nel canvas
                    UtilityRect(panMaterial, 10);
                    bttnSubScript.matPanel = panMaterial;

                    //Creeiamo i button linkati ai vari materiali e settiamoli figli del panMaterial
                    InstanziaMaterialButton(fam.nome + "\\" + subFam.nome, panMaterial);

                    //Disattiviamo il panel material e il panel sottofamiglia per concludere l'operazione
                    panMaterial.SetActive(false);
                    subFamPanel.SetActive(false);
                }
            }
            else //Se la famiglia non ha una sottofamiglia occorre instanziare i button dei materiali al panel subfamiglia creato al primo punto
            {
                InstanziaMaterialButton(fam.nome, subFamPanel);
            }
        }
    }

    /// <summary>
    /// Questa funzione ci permette di settare valori del rect trasform 
    /// </summary>
    /// <param name="pannello">Il pannello che deve essere settato</param>
    /// <param name="valore">Il valore che vogliamo dare ad ogni lato tranne al right</param>
    public void UtilityRect(GameObject pannello, float valore)
    {
        RectTransformExtensions.SetLeft(pannello.GetComponent<RectTransform>(), valore);
        RectTransformExtensions.SetTop(pannello.GetComponent<RectTransform>(), valore);
        RectTransformExtensions.SetRight(pannello.GetComponent<RectTransform>(), 0);
        RectTransformExtensions.SetBottom(pannello.GetComponent<RectTransform>(), valore);
        pannello.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
    }

    /// <summary>
    /// Questa funzione crea i button material e li applica al giusto panel
    /// </summary>
    /// <param name="folder">La Cartella dove il programma va a cercare eventuali materiali</param>
    /// <param name="pannello">Il pannello alla quale deve applicarei button come figli</param>
    public void InstanziaMaterialButton(string folder, GameObject pannello)
    {
        var materialiFamiglia = Resources.LoadAll<Material>(folder);
        if (materialiFamiglia != null)
        {
            foreach (Material materialeSott in materialiFamiglia)
            {
                var bttnMat = Instantiate(buttonMaterial);
                bttnMat.name = "Button" + materialeSott.name;
                bttnMat.transform.SetParent(pannello.transform);
                bttnMat.GetComponent<ButtonMateriale>().materiale = materialeSott;
                //bttnMat.GetComponent<ButtonMateriale>().setMaterialColorButton(); //Questo setta il button con il colore o la texture principale del materiale
                bttnMat.GetComponentInChildren<Text>().text = materialeSott.name;
                bttnMat.GetComponent<Button>().onClick.AddListener(() => Camera.main.GetComponent<AudioSource>().Play());
            }
            if(materialiFamiglia.Length > 15) //Se abbiamo più di 15 materiali all'interno della cartella abbiamo bisogno di settare ContentSizeFter in MinSize e abilitare lo scrollRect
            {
                pannello.GetComponent<ContentSizeFitter>().verticalFit = ContentSizeFitter.FitMode.MinSize;
                pannello.GetComponent<ScrollRect>().enabled = true;
                //pannello.GetComponent<RectTransform>().anchoredPosition = new Vector2(10.0f, pannello.GetComponent<RectTransform>().sizeDelta.y / 2f);
            }
        }
        else
        {
            Debug.LogWarning("Nessuna famiglia e nessun materiale all'interno della cartella " + folder);
        }
    }

    /// <summary>
    /// Questa funzione chiude tutti i pannelli sottofamiglia/materiali e anche famiglia se il parametro bool è true, inoltre chiude il button back
    /// </summary>
    /// <param name="ancheFamiglia"></param>
    public void CloseAnyWindow(bool ancheFamiglia)
    {
        if(ancheFamiglia)
        {
            panelFamiglieGO.SetActive(false);
            if(oggettoAttivo != null)
            {
                oggettoAttivo.GetComponent<cakeslice.Outline>().enabled = false;
            }
            buttonExitGO.SetActive(false);
        }

        foreach (GameObject pan in pannelliSub)
        {
            pan.SetActive(false);
        }

        foreach (GameObject pan in pannelliMaterial)
        {
            pan.SetActive(false);
        }
        backButton.SetActive(false);
    }

    /// <summary>
    /// Questa funzione serve semplicemente ad avviare l'animazione UI del menu
    /// </summary>
    public void SwitchMenu()
    {
        animatore.SetTrigger("Menu");
    }

    /// <summary>
    /// Questa funzione setta l'animazione del mobile su Aperto
    /// </summary>
    public void Aperto()
    {
        if(modelloAttivo != null)
        {
            if(modelloAttivo.GetComponent<Animation>())
            {
                var anim = modelloAttivo.GetComponent<Animation>().Play("on");
            }
        }
    }

    /// <summary>
    /// Questa funzione setta l'animazione del mobile su Chiuso
    /// </summary>
    public void Chiuso()
    {
        if (modelloAttivo != null)
        {
            if (modelloAttivo.GetComponent<Animation>())
            {
                var anim = modelloAttivo.GetComponent<Animation>().Play("off");
            }
        }
    }


    /// <summary>
    /// Questa funzione va abbinata al buttonback e serve per tornare al pannello famiglia se ci si trova nel pannello sottofamiglia e al pannello sottofamiglia se ci si trova in un pannello materiali
    /// </summary>
    public void BackAction()
    {
        if(sonoInSubFam) //Attualmente sono in un menu sottofamiglia? Se si devo ritornare al menu famiglia
        {
            sonoInSubFam = false;
            subFamAttuale.SetActive(false);
            panelFamiglieGO.SetActive(true);
            backButton.SetActive(false);
            subFamAttuale.SetActive(false);
        }
        else //Se no, vuole dire che sono ad un menu di scelta dei materiali e perciò dovrò tornare al pannello Sottofamiglia
        {
            sonoInSubFam = true;
            subFamAttuale.SetActive(true);
            materialPanelAttuale.SetActive(false);
        }
    }

    /// <summary>
    /// Questa funzione chiude l'applicazione
    /// </summary>
    public void ChiudiApp()
    {
        Application.Quit();
    }

    /// <summary>
    /// Questa funzione ricarica la scena attuale
    /// </summary>
    public void ResetScene()
    {
        SceneManager.LoadScene(1);
    }
}
