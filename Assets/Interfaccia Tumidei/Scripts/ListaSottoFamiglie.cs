﻿using UnityEngine;

[System.Serializable]
public class ListaSottoFamiglie
{
    [HideInInspector]
    public string nome;
    public bool compatibile;

    public ListaSottoFamiglie(string nom, bool comp)
    {
        nome = nom;
        compatibile = comp;
    }
}
