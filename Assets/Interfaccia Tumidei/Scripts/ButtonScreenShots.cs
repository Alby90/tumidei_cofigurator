﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

/// <summary>
/// Questa classe serve semplicemente a creare
/// </summary>
class ButtonScreenShots : MonoBehaviour
{
    public string nameFolder;

    public void Start()
    {
        if(!Directory.Exists(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop) + "\\"+nameFolder))
        {
            Directory.CreateDirectory(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop) + "\\"+nameFolder);
        }
    }

    public void Scatta()
    {
        StartCoroutine(CaptureScreen());
    }

    public IEnumerator CaptureScreen()
    {
        var outlineAttiva = false; //Questa variabile ci serve per ricordare se c'è un outline attiva nella scena
        // Wait till the last possible moment before screen rendering to hide the UI
        yield return null;
        Manager.instance.canvasPrincipale.GetComponent<Canvas>().enabled = false;
        if(Manager.instance.oggettoAttivo != null)//Se abbiamo un oggetto attivo...
        {
            if(Manager.instance.oggettoAttivo.GetComponent<cakeslice.Outline>().enabled)//...se questo oggetto ha l'outline attivato
            {
                outlineAttiva = true; //Settiamo la variabile outlineAttiva come true...
                Manager.instance.oggettoAttivo.GetComponent<cakeslice.Outline>().enabled = false; //...e spegniamo l'outline.
            }
        }

        // Wait for screen rendering to complete
        yield return new WaitForEndOfFrame();

        // Take screenshot
        var nameFile = System.IO.Path.Combine(
        System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop) + "\\"+nameFolder+"\\" +
        "Tumidei" + System.DateTime.Today.Month.ToString() + System.DateTime.Now.Day.ToString() +
        System.DateTime.Now.Hour.ToString() +
        System.DateTime.Now.Minute.ToString() +
        System.DateTime.Now.Second.ToString() + ".png");
        ScreenCapture.CaptureScreenshot(nameFile);
        print("ScreenShot scattato: "+ nameFile);

        // Show UI after we're done
        Manager.instance.canvasPrincipale.GetComponent<Canvas>().enabled = true;

        if (outlineAttiva) //Se la variabile Outline è true
        {
            Manager.instance.oggettoAttivo.GetComponent<cakeslice.Outline>().enabled = true; //Vuole dire che all'oggetto attualmente selezionato occorre "accendere" l'outline una volta scattata la foto
        }
    }
}
