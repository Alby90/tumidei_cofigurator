﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode, RequireComponent(typeof(cakeslice.Outline), typeof(BoxCollider))]
public class Modificabile : MonoBehaviour
{
    cakeslice.Outline outl;

    public List<ListaFamiglie> listFam;

    //Applichiamo a tutti i selezionabili l'outline
    public void Start()
    {
        //Controlliamo in start che la dimensione della lista famiglie compatibili sia in linea con quella del manager
        //ControllaDimensione();

        outl = GetComponent<cakeslice.Outline>();
        outl.enabled = false;
        ControllaDimensione(ManagerFamiglie.instance.famiglie);
    }

    private void OnEnable()
    {
        ManagerFamiglie.aggiorna += ControllaDimensione;
    }

    void ControllaDimensione(List<Famiglia> fam)
    {
        /*if*/while (fam.Count!=listFam.Count/*fam.Count != this.listFam.Count*/)
        {
            if (fam.Count > this.listFam.Count) //Se nel Manager Famiglie ci son più famiglie che nel Modificabile...
            {
                for (int i = this.listFam.Count; i < fam.Count; i++)
                {
                    this.listFam.Add(new ListaFamiglie(fam[i].nome, false));
                }
            }
            else if (fam.Count < this.listFam.Count) //Se nel Manager Famiglie ci son meno famiglie che nel Modificabile...
            {
                for (int i = this.listFam.Count; i > fam.Count; i--)
                {
                    this.listFam.RemoveAt(listFam.Count - 1);
                }
            }

            for (int i = 0; i < this.listFam.Count; i++) //Aggiorniamo i nomi delle varie famiglie
            {
                if (this.listFam[i].nome != fam[i].nome)
                {
                    this.listFam[i].nome = fam[i].nome;
                }
            }
        }


        /*//Qui adesso analizziamo tutte le sottofamiglie delle famiglie del manager e le sincronizziamo con le famiglie del Modificabile
        for (int i = 0; i < fam.Count; i++) //controlliamo tutte le famiglie
        {
            while(fam[i].sottofamiglie.Count != listFam[i].sottoFamiglie.Count)
            {
                if (fam[i].sottofamiglie.Count > listFam[i].sottoFamiglie.Count)
                {
                    this.listFam[i].sottoFamiglie.Add(new ListaSottoFamiglie("fam", false));
                }
                else if (fam[i].sottofamiglie.Count < listFam[i].sottoFamiglie.Count)
                {
                    this.listFam[i].sottoFamiglie.RemoveAt(listFam.Count - 1);
                }
            }

            //Con questo script aggiorniamo i nomi delle sottofamiglie
            for(int j = 0; j< listFam[i].sottoFamiglie.Count; j++)
            {
                if (listFam[i].sottoFamiglie[j].nome != fam[i].sottofamiglie[j].nome)
                {
                    listFam[i].sottoFamiglie[j].nome = fam[i].sottofamiglie[j].nome;
                }
            }
        }*/
    }
}
