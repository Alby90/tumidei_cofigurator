﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Famiglia
{
    public string nome;
    public List<Sottofamiglia> sottofamiglie;
}
